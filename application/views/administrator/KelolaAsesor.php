<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
<div class="col-12">

<!-- Column -->
<div class="card"  style="margin-top: 30px; border: 0px; box-shadow: 1px 2px 2px 2px #e6e6e6; border-radius: 8px;">
<div class="card-body">

<div class="alert alert-success hide_msg pull" style="width: 100%; background: #feb390; color: white"> <i class="fa fa-check-circle"></i> Selamat Datang &nbsp;
<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
</div>

<h4 class="card-title">Daftar Asesor</h4>
<h6 class="card-subtitle">Just click on word which you want to change and enter</h6>

<div style="margin-bottom: 10px;">
<a href="<?php echo base_url('administrator/KelolaAsesorTambah') ?>" style="background: #f3f3f3; color: #699cfc; border: 0;"" class="btn btn-success">
Tambah</a>
</div>

<table class="table table-striped table-bordered">
<thead>
<tr>
<th>#</th>
<th>Nama</th>
<th>Prodi</th>
<th>Hak Akses</th>
<th>Persetujuan</th>
<th>Aksi</th>
<th> </th>
</tr>
</thead>
<tbody>
<tr id="1" class="gradeX">
<td>1</td>
<td>A. Rahman</td>
<td>Informatika</td>
<td>Asesor I</td>
<td><a href="#" style="color: red">Batal</a></td>
<td><button style="background: #f3f3f3; color: black; border: 0;"" class="btn btn-success">
Edit</button></td>
<td><button style="background: #f3f3f3; color: black; border: 0;"" class="btn btn-success">
Hapus</button></td>


</tr>

<tr id="1" class="gradeX">
<td>2</td>
<td>I Made Widiarta</td>
<td>Informatika</td>
<td>Asesor II</td>
<td><a href="#" style="color: #699cfc">Setuju</a></td>
<td><button style="background: #f3f3f3; color: black; border: 0;"" class="btn btn-success">
Edit</button></td>
<td><button style="background: #f3f3f3; color: black; border: 0;"" class="btn btn-success">
Hapus</button></td>


</tr>

</tbody>
<tfoot>
<tr>
<th>#</th>
<th>Nama</th>
<th>Prodi</th>
<th>Hak Akses</th>
<th>Persetujuan</th>
<th>Aksi</th>
<th> </th>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->