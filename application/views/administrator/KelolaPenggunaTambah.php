<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
<div class="col-12">

<!-- Column -->
<div class="card"  style="margin-top: 30px; border: 0px; box-shadow: 1px 2px 2px 2px #e6e6e6; border-radius: 8px;">
<div class="card-body">

<div class="alert alert-success hide_msg pull" style="width: 100%; background: #feb390; color: white"> <i class="fa fa-check-circle"></i> Selamat Datang &nbsp;
<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
</div>

<h4 class="card-title">Tambah Pengguna</h4>
<h6 class="card-subtitle">Just click on word which you want to change and enter</h6>


<table class="table">
<thead>
<tr>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>

<tr id="1" class="gradeX">
<td>
<img style="width: 150px; height: 150px;" src="<?php echo base_url() ?>assets/images/Username-512.png">
</td>

<td>
<div class="form-group">
<label class="control-label">Nama Lengkap</label>
<input type="text" id="bidangIlmu" class="form-control">
</div>
</td>

<td>
<div class="form-group">
<label class="control-label">Username</label>
<input type="text" id="bidangIlmu" class="form-control">
</div>
</td>


<tr id="1" class="gradeX">
<td>
<div class="form-group">
<label class="control-label">Upload Foto</label>
<input type="file" name="file" class="form-control" required="" aria-invalid="false">
</div>
</td>

<td>
<div class="form-group">
<label class="control-label">Jabatan</label>
<input type="text" id="bidangIlmu" class="form-control">
</div>
</td>

<td>
<div class="form-group">
<label class="control-label">Password</label>
<input type="text" id="bidangIlmu" class="form-control">
</div>
</td>


<tr id="1" class="gradeX">
<td>

</td>

<td>
<div class="form-group">
<label class="control-label">Hak Akses</label>
<select class="form-control custom-select" aria-invalid="false">
<option></option>
<option>Administrator</option>
<option>Kaprodi</option>

</select>
</div>
</td>

<td>

</td>

</tbody>
</table>

<div>
<div class="col-md-6">
<div class="row">
<div class="col-md-offset-3 col-md-9">
<button type="submit" class="btn btn-success">Simpan</button>
<button type="button" class="btn btn-inverse">Batal</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->