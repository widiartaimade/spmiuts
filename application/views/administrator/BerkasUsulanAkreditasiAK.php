<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
           
            <!-- Column -->
            <div class="card"  style="margin-top: 30px; border: 0px; box-shadow: 1px 2px 2px 2px #e6e6e6; border-radius: 8px;">
                <div class="card-body">

                    <div class="alert alert-success hide_msg pull" style="width: 100%; background: #feb390; color: white"> <i class="fa fa-check-circle"></i> Selamat Datang &nbsp;
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>

                    <h4 class="card-title">
Berkas Usulan Akreditasi</h4>
                    <h6 class="card-subtitle">Just click on word which you want to change and enter</h6>

 <table class="table">
<thead>
<tr>
<th></th>
<th></th>
</tr>
</thead>
<tbody>

<tr id="1" class="gradeX">

<td>Program Studi</td>
<td>Teknik Informatika</td>

</tr>

<tr id="2" class="gradeA">

<td>Perguruan Tinggi</td>
<td>Universitas Teknologi Sumbawa</td>

</tr>


<tr id="2" class="gradeA">

<td>Berkas Penilaian</td>
<td><button style="background: #f3f3f3; color: black; border: 0; width: 30%" class="btn btn-success">
Download</button></td>

</tr>


<tr id="3" class="gradeA">

<td>Upload Berkas</td>
<td></td>

</tr>

<tr id="3" class="gradeA">

<td>Berkas Penilaian</td>
<td><input type="file" name="file" class="form-control" required="" aria-invalid="false">
<p style="margin-top: 10px"><button style="background: #f3f3f3; color: black; border: 0; width: 30%" class="btn btn-success">
Download</button></p>
</td>

</tr>

<tr id="3" class="gradeA">

<td>Rentang Tanggal AL</td>
<td>20-02-2019 - 28-02-2019</td>

</tr>



<tr id="4" class="gradeA">

<td>Usul Tanggal</td>
<td>
  <div class="form-group">
<input type="date" class="form-control" placeholder="dd/mm/yyyy" aria-invalid="false">

<p style="margin-top: 10px"><button style="background: #f3f3f3; color: black; border: 0; width: 30%" class="btn btn-success">Usulan Tanggal</button></p>
                        </div>

</td>

</tr>

                            
                            
                        </tbody>
            
                    </table>



                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->