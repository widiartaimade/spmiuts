<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
<div class="col-12">

<!-- Column -->
<div class="card"  style="margin-top: 30px; border: 0px; box-shadow: 1px 2px 2px 2px #e6e6e6; border-radius: 8px;">
<div class="card-body">

<div class="alert alert-success hide_msg pull" style="width: 100%; background: #feb390; color: white"> <i class="fa fa-check-circle"></i> Selamat Datang &nbsp;
<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
</div>

<h4 class="card-title">Tambah Asesor</h4>
<h6 class="card-subtitle">Just click on word which you want to change and enter</h6>


<table class="table table-striped table-bordered">
<thead>
<tr>
<th></th>
</tr>
</thead>
<tbody>
<tr id="1" class="gradeX">
<td>
<div class="col-md-6">
<div class="form-group validate">
<label>Pilih Asesor</label>
<select class="form-control custom-select" aria-invalid="false">
<option></option>
<option>A. Rahman</option>
<option>I Made Widiarta</option>
</select>
</div>
</div>

<div class="col-md-6">
<div class="form-group validate">
<label>Pilih Asesor Sebagai</label>
<select class="form-control custom-select" aria-invalid="false">
<option></option>
<option>Asesor I</option>
<option>Asesor II</option>
</select>
</div>
</div>

<div class="col-md-6">
<div class="form-group validate">
<label>Pilih Prodi</label>
<select class="form-control custom-select" aria-invalid="false">
<option></option>
<option>Informatika</option>
<option>Sipil</option>
</select>
</div>
</div>


</td>


</tbody>
</table>

<div>
<div class="col-md-6">
<div class="row">
<div class="col-md-offset-3 col-md-9">
<button type="submit" class="btn btn-success">Simpan</button>
<button type="button" class="btn btn-inverse">Batal</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->