<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
           
            <!-- Column -->
            <div class="card"  style="margin-top: 30px; border: 0px; box-shadow: 1px 2px 2px 2px #e6e6e6; border-radius: 8px;">
                <div class="card-body">

                    <div class="alert alert-success hide_msg pull" style="width: 100%; background: #feb390; color: white"> <i class="fa fa-check-circle"></i> Selamat Datang &nbsp;
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>

                    <h4 class="card-title">Daftar Penugasan Asesmen</h4>
                    <h6 class="card-subtitle">Just click on word which you want to change and enter</h6>
                    <table class="table table-striped table-bordered" id="editable-datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Prodi</th>
                                <th>Perguruan Tinggi</th>
                                <th>Program</th>
                                <th>Batas Akhir AL</th>
                                <th>Priode AL</th>
                                <th>Status Split</th>
                                <th>Usul Tanggal AL</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr id="1" class="gradeX">
                                <td>1</td>
                                <td><a href="<?php echo base_url('asesor/BerkasUsulanAkreditasi_Penugasan') ?>">Informatika</a></td>
                                <td>Universitas Teknologi Sumbawa</td>
                                <td>S1</td>
                                <td>15-02-2019</td>
                                <td>20-02-2019 - 28-02-2019</td>
                                <td><a href="<?php echo base_url('asesor/CekSplit') ?>">Split</a></td>
                                <td>24-02-2019</td>
                            </tr>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Nama Prodi</th>
                                <th>Perguruan Tinggi</th>
                                <th>Program</th>
                                <th>Batas Akhir AL</th>
                                <th>Priode AL</th>
                                <th>Status Split</th>
                                <th>Usul Tanggal AL</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->