<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
           
            <!-- Column -->
            <div class="card"  style="margin-top: 30px; border: 0px; box-shadow: 1px 2px 2px 2px #e6e6e6; border-radius: 8px;">
                <div class="card-body">

                    <div class="alert alert-success hide_msg pull" style="width: 100%; background: #feb390; color: white"> <i class="fa fa-check-circle"></i> Selamat Datang &nbsp;
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>

                    <h4 class="card-title">Daftar Penawaran Asesmen</h4>
                    <h6 class="card-subtitle">Just click on word which you want to change and enter</h6>
                    <table class="table table-striped table-bordered" id="editable-datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Prodi</th>
                                <th>Perguruan Tinggi</th>
                                <th colspan="2" align="center">Respon</th>
                              
                                <th>Batas Akhir AK</th>
                                <th>Batas Awal AL</th>
                                <th>Batas Akhir AL</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr id="1" class="gradeX">
                                <td>1</td>
                                <td>Informatika</td>
                                <td>Universitas Teknologi Sumbawa</td>

                                <td class="center"><button style="background: #699cfc; border: 0; width: 100%" type="submit" class="btn btn-success">Unggah</button></td>
                                <td class="center"><button style="background: #fc363b; border: 0; width: 100%" type="submit" class="btn btn-success">Tolak</button></td>

                                 <td class="center">02-01-2019</td>
                                 <td class="center">10-01-2019</td>
                                 <td class="center">20-01-2019</td>
                            </tr>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Nama Prodi</th>
                                <th>Perguruan Tinggi</th>
                                <th colspan="2">Respon</th>
                              
                                <th>Batas Akhir AK</th>
                                <th>Batas Awal AL</th>
                                <th>Batas Akhir AL</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->