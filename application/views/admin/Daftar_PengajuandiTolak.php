<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
           
            <!-- Column -->
            <div class="card"  style="margin-top: 30px; border: 0px; box-shadow: 1px 2px 2px 2px #e6e6e6; border-radius: 8px;">
                <div class="card-body">

                    <div class="alert alert-success hide_msg pull" style="width: 100%; background: #feb390; color: white"> <i class="fa fa-check-circle"></i> Selamat Datang &nbsp;
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>

                    <h4 class="card-title">Daftar Pengajuan di Tolak [Kosong]</h4>
                    <h6 class="card-subtitle">Just click on word which you want to change and enter</h6>
                    <table class="table table-striped table-bordered" id="editable-datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Jenis</th>
                                <th>Prodi / Perguruan Tinggi</th>
                                <th>Tanggal Pengajuan / Tanggal Diterima</th>
                                <th>Status</th>
                                <th>Menu</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr id="1" class="gradeX">
                                <td>0</td>
                                <td> </td>
                                <td> </td>
                                <td class="center"> </td>
                                <td class="center"> </td>
                                 <td class="center"> </td>
                            </tr>

                            
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Jenis</th>
                                <th>Prodi / Perguruan Tinggi</th>
                                <th>Tanggal Pengajuan / Tanggal Diterima</th>
                                <th>Status</th>
                                <th>Menu</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->