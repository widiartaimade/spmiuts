<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->

<script
src="https://code.jquery.com/jquery-2.1.4.js"
integrity="sha256-siFczlgw4jULnUICcdm9gjQPZkw/YPDqhQ9+nAOScE4="
crossorigin="anonymous"></script>


<script language="javascript">
$(document).ready(function(){

$("#tabeldata").hide();
$("#tabeldatadua").hide();
$("#tabeldatatiga").hide();

$("#muncul").click(function(){
$("#tabeldata").show();
$("#tabeldatadua").show();
$("#tabeldatatiga").show();

});
});
</script>


<div class="container-fluid">
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
<div class="col-12">

<!-- Column -->
<div class="card"  style="margin-top: 30px; border: 0px; box-shadow: 1px 2px 2px 2px #e6e6e6; border-radius: 8px;">
<div class="card-body">

<div class="alert alert-success hide_msg pull" style="width: 100%; background: #feb390; color: white"> <i class="fa fa-check-circle"></i> Selamat Datang &nbsp;
<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
</div>

<h4 class="card-title">
Unduh template borang</h4>
<h6 class="card-subtitle">Ikuti petunjuk sesuai yang tertulis di bawah ini.</h6>

<p>
Template Borang untuk Pengajuan Akreditasi
Setiap pengajuan usulan akreditasi perguruan tinggi (APT), mesti melampirkan dokumen berikut: </p>
<ol>
<li> Surat pengantar dari Pimpinan Institusi </li>
<li> Surat Pernyataan </li>
<li> Buku III (Borang Institusi) </li> 
<li> Lampirkan Borang Institusi (sesuai Buku III) </li>
<li> Evaluasi Diri </li>
<li> Borang Data Kuantitatif (sesuai template BAN-PT) </li> 
</ol>

<p>Sedangkan, kelengkapan dokumen untuk pengajuan akreditasi program sutid (S1, S2, S3, Diploma) adalah: </p>

<ol>
<li> Surat Pengantar dari Ketua Program Studi </li>
<li> Surat Pernyataan </li>
<li> Buku IIIA (Borang Program Studi) </li> 
<li> Lampiran Borang Program Studi (sesuai Buku IIIA) </li> 
<li> Buku IIIB (Borang Unit Pengelola Program Studi) </li>
<li> Lampiran Borang Unit Pengelola Program Studi (sesuai Buku IIIB) </li>
<li> Evaluasi Diri </li> 
<li> Borang Data Kuantitatif (sesuai template BAN-PT) </li>
</ol>

<p>Untuk Borang Data Kuantitatif, BAN-PT hanya menerima borang dengan template berikut: </p>
<ol>
<li> Borang Akreditasi Perguuan Tinggi (APT) </li>
<li> Borang Akreditasi Prodi (AP) S1 </li>
<li> Borang Akreditasi Prodi (AP) S2 </li>
<li> Borang Akreditasi Prodi (AP) S3 </li>
<li> Borang Akreditasi Prodi (AP) Diploma </li>
</ol>

<p>Silahkan isi dengan lengkap dan unggah ketika mengajukan usulan akreditasi
</p>
</div>
</div>
</div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
