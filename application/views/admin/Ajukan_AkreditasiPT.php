<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->

<script
src="https://code.jquery.com/jquery-2.1.4.js"
integrity="sha256-siFczlgw4jULnUICcdm9gjQPZkw/YPDqhQ9+nAOScE4="
crossorigin="anonymous"></script>


<script language="javascript">
$(document).ready(function(){

$("#tabeldata").hide();
$("#tabeldatadua").hide();
$("#tabeldatatiga").hide();

$("#muncul").click(function(){
$("#tabeldata").show();
$("#tabeldatadua").show();
$("#tabeldatatiga").show();

});
});
</script>


<div class="container-fluid">
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
<div class="col-12">

<!-- Column -->
<div class="card"  style="margin-top: 30px; border: 0px; box-shadow: 1px 2px 2px 2px #e6e6e6; border-radius: 8px;">
<div class="card-body">

<div class="alert alert-success hide_msg pull" style="width: 100%; background: #feb390; color: white"> <i class="fa fa-check-circle"></i> Selamat Datang &nbsp;
<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
</div>

<h4 class="card-title">
Ajukan Akreditasi Perguruan Tinggi</h4>
<h6 class="card-subtitle">Just click on word which you want to change and enter</h6>

<table class="table">
<thead>
<tr>
<th></th>
<th></th>
</tr>
</thead>
<tbody>

<tr id="1" class="gradeX">

<td>Akreditasi/Reakreditasi</td>
<td>
    
<div class="form-check">
<label class="custom-control custom-radio">
<input id="radio1" name="radio" type="radio" checked="" class="custom-control-input" aria-invalid="false">
<span class="custom-control-indicator"></span>
<span class="custom-control-description">Akreditasi Pertama</span>
</label>
<label class="custom-control custom-radio">
<input id="radio2" name="radio" type="radio" class="custom-control-input" aria-invalid="false">
<span class="custom-control-indicator"></span>
<span class="custom-control-description">Reakreditasi</span>
</label>
</div>
</td>

</tr>

<tr id="1" class="gradeX">


<tr id="1" class="gradeX">

<td>Nomor Surat Pengantar</td>
<td> <input type="text" id="bidangIlmu" class="form-control"></td>

</tr>


<tr id="2" class="gradeA">

<td>Tanggal Surat Pengantar</td>
<td> <input type="date" class="form-control" placeholder="dd/mm/yyyy" aria-invalid="false"> </td>

</tr>


<tr id="3" class="gradeA">

<td>Alamat Pos</td>
<td><input type="text" id="Apos" class="form-control"></td>

<td>Kode Pos</td>
<td><input type="text" id="Kopos" class="form-control"></td>

</tr>




</tr>

<tr id="4" class="gradeA">

<td></td>
<td>
<div class="form-group">
<button style="background: #282828; border: 0; width: 100%" type="submit" class="btn btn-success" id="muncul">
Ajukan dan Lanjut ke Unggah Berkas</button>
</div>

</td>

</tr>

<tr id="tabeldata" class="gradeA">

<td>Catatan BAN-PT</td>
<td>
<div class="form-group">
Belum Ada Catatan
</div>

</td>

</tr>



</tbody>

</table>

<table id="tabeldatadua" class="table table-striped table-bordered" id="editable-datatable">
<thead>
<tr>
<th>#</th>
<th>Nama Dokumen (klik untuk melihat dokumen)</th>
<th>Format</th>
<th>Pilih Berkas Baru</th>
<th colspan="2">Menu</th>
<th></th>
</tr>
</thead>
<tbody>
<tr id="1" class="gradeX">
<td>1</td>
<td>Contoh Dokumen I</td>
<td>PDF</td>
<td class="center"><input type="file" name="file" class="form-control" required="" aria-invalid="false"></td>
<td class="center"><button style="background: #282828; border: 0; width: 100%" type="submit" class="btn btn-success">
Unggah</button></td>
<td class="center"><button style="background: #282828; border: 0; width: 100%" type="submit" class="btn btn-success">
Hapus</button></td>
</tr>



</tbody>
<tfoot>
<tr>
<th>#</th>
<th>Nama Dokumen (klik untuk melihat dokumen)</th>
<th>Format</th>
<th>Pilih Berkas Baru</th>
<th colspan="2">Menu</th>
<th></th>
</tr>
</tfoot>
</table>

<button id="tabeldatatiga" style="background: #282828; border: 0; width: 100%" type="submit" class="btn btn-success">
Batalkan Pengajuan (seluruh dokumen akan dihapus)</button>
</div>
</div>
</div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
