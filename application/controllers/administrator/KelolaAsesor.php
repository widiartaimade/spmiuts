<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class KelolaAsesor extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->model('common_model');
    }

    
    public function index(){
        $data = array();

        $data['main_content'] = $this->load->view('administrator/KelolaAsesor', $data, TRUE);
        $this->load->view('administrator/index', $data);
    }


}