<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajukan_AkreditasiProdi_Lanjut extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->model('common_model');
    }
 
    public function index(){
        $data = array();
        $data['main_content'] = $this->load->view('admin/Ajukan_AkreditasiProdi_Lanjut', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

}