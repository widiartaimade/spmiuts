<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BerkasUsulanAkreditasiAK extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->model('common_model');
    }

    
    public function index(){
        $data = array();

        $data['main_content'] = $this->load->view('asesor/BerkasUsulanAkreditasiAK', $data, TRUE);
        $this->load->view('asesor/index', $data);
    }


}