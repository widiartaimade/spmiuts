<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BerkasUsulanAkreditasi_Penugasan extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->model('common_model');
    }

    
    public function index(){
        $data = array();

        $data['main_content'] = $this->load->view('asesor/BerkasUsulanAkreditasi_Penugasan', $data, TRUE);
        $this->load->view('asesor/index', $data);
    }


}